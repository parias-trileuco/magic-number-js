INSTALL = ./bin/install.sh --yes
UNINSTALL = ./bin/uninstall.sh --yes
TEST_BACKEND = npm test

run-tests:
	$(TEST_BACKEND)

install:
	$(INSTALL)

uninstall:
	$(UNINSTALL)

reinstall:
	@make uninstall
	@make install

