// MagicNumber Algorithm Import
const { getMagicNumber, getMagicNumberMapping } = require('../magicNumberAlgorithm');
const assert = require('assert');


describe('Magic Number', function () {
    const magicNumberMapping = getMagicNumberMapping(9999);
    const magicNumber = getMagicNumber(8597);
    const expectedMagicNumber = 6174;
    const expectedTotalSteps = 7;
    const expectedResolutionTimeLowerLimit = 0.04;
    const expectedResolutionTimeUpperLimit = 1
      
    const expectedStep3Count = 2400
    const expectedStep4Count = 1272
    const expectedStep5Count = 1446
    const expectedStep6Count = 1656
    const expectedStep7Count = 2184

    it('It should return a proper Step value.', function () {
        try {
            assert.equal(magicNumber[0], expectedTotalSteps);
            } catch (error) {
            console.error(error.message);
            } 
        });

    it('It should return a proper MagicNumber value.', function () {
      try {   
        assert.equal(magicNumber[1], expectedMagicNumber);
      } catch (error) {
        console.error(error.message);
      } 
    });

    it('It should return a proper Resolution Time value.', async function () {
      try {   
        assert.equal(expectedResolutionTimeLowerLimit <= magicNumber[2], magicNumber[2] <= expectedResolutionTimeUpperLimit);
      } catch (error) {
        console.error(error.message);
      } 
    });

        it('It should return the proper step values for 1 to 9999.', async function () {
      try {
        assert.equal(magicNumberMapping[0]['3'][0], expectedStep3Count);
        assert.equal(magicNumberMapping[0]['4'][0], expectedStep4Count);
        assert.equal(magicNumberMapping[0]['5'][0], expectedStep5Count);
        assert.equal(magicNumberMapping[0]['6'][0], expectedStep6Count);
        assert.equal(magicNumberMapping[0]['7'][0], expectedStep7Count);

      } catch (error) {
        console.error(error.message);
      } 
    });
});