// MagicNumber Algorithm Import
const { getMagicNumber } = require('./magicNumberAlgorithm');


var numberArg = process.argv.slice(2);
if (numberArg.length !== 0 || numberArg <= 9999) {
    getMagicNumber(numberArg, 4);
} else {
    console.log('Sorry, that is not something I know how to do. Try node magicNumber.js <number>.')
}

// Establecer el numero de numeros que se usan para el magic number, por defecto = 4
// hacer el padding siempre si fuese 5300 0035

