// MagicNumber Utils Module Import
const { lowestNum, greatestNum, zeroPad, displayStep, displayHeader, displayFooter } = require("./magicNumberUtils");

// Performance Import: time the execution
const { performance } = require('perf_hooks');

 /**
  * @abstract magicNum: This function, will calculate the MagicNumber using Tail Recursion.
  * @param {Integer} num number to use for the MagicNumber
  */
 function magicNum(num, numberLength) {
    var greatestNumber = zeroPad(greatestNum(num), numberLength);
    var lowestNumber = zeroPad(lowestNum(num), numberLength);
    var magicNumber = zeroPad(greatestNumber - lowestNumber, numberLength);

    displayStep(0, num, greatestNumber, lowestNumber, magicNumber, numberLength);
    return recursiveMagicNumber(magicNumber, 1, numberLength);
}

/**
 * @abstract recursiveMagicNumber: This function, will calculate the MagicNumber using Tail Recursion.
 * @param {Integer} magicNumber current magic number value.
 * @param {Integer} currentStep current step counter value.
 * @param {Integer} numberLenght lenght of the fst number, used on padding
 */
function recursiveMagicNumber(magicNumber, currentStep, numberLength) {
    var _newGreatestNumber = zeroPad(greatestNum(magicNumber), numberLength)
    var _newLowestNumber = zeroPad(lowestNum(magicNumber), numberLength)
    var _newMagicNumber =  zeroPad(_newGreatestNumber - _newLowestNumber, numberLength)

    displayStep(currentStep, magicNumber, _newGreatestNumber, _newLowestNumber, _newMagicNumber, numberLength)
    if (_newMagicNumber === magicNumber) {
        return [currentStep, magicNumber]
    }
    return recursiveMagicNumber(_newMagicNumber, currentStep + 1, numberLength)
}

module.exports = {
 /**
  * @abstract getMagicNumber: This function, will calculate the MagicNumber for any given integer.
  * @param {Integer} num 
  */
    getMagicNumber: function getMagicNumber(num, numberLength=4) {
        displayHeader();
        var t0 = performance.now();
        var magicNumberResult = magicNum(num, numberLength);
        var t1 = performance.now();
        var totalSteps = magicNumberResult[0];
        var magicNumber = magicNumberResult[1];
        var totalResolutionTime = t1 - t0;
  

        return [totalSteps, magicNumber, totalResolutionTime]
    },

   /**
     * @abstract getMagicNumber: This function, will calculate the MagicNumber for any given integer.
     * @param {Integer} num 
     */
    getMagicNumberMapping: function getMagicNumberMapping(num) {
        displayHeader();
        var magicNumberMapping = new Object();
        var t0 = performance.now();
        for (i = 1; i < num; i++) {
            var magicNumberResult = magicNum(zeroPad(i, 4));
            var totalSteps = magicNumberResult[0];
            var magicNumber = magicNumberResult[1];
            magicNumberMapping[zeroPad(i, 4)] = [totalSteps, magicNumber]
            
        }

        var filteredMagicNumber = new Object();
        for (const [key, value] of Object.entries(magicNumberMapping)) {
            if (value[0] > 2) {
                if (filteredMagicNumber[value[0]] === undefined) {
                    filteredMagicNumber[value[0]] = [1 , [key]];
                } else {
                    filteredMagicNumber[value[0]][0] = filteredMagicNumber[value[0]][0] + 1;
                    filteredMagicNumber[value[0]][1].push(key);
                }
               
            }
        }
        var t1 = performance.now();
        var totalResolutionTime = t1 - t0;

        displayFooter(num, totalResolutionTime, totalSteps, magicNumber);
        return [filteredMagicNumber, totalResolutionTime]
 
    },
}


