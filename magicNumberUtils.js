module.exports = {
    /**
     * @abstract zeroPad: This function, will padd the proper number of 0 while using displayStep function.
     * @param {Integer} num current number value
     * @param {Integer} places space to be filled
     */
    zeroPad: function zeroPad(num, places) {
        return String(num).padStart(places, '0')
    }
    ,
    /**
     * @abstract lowestNum: This function, will retrieve the lowest posible number from a integer.
     * @param {Integer} num current number value
     */
    lowestNum: function lowestNum(num) {
        return (
            parseInt(num.toString()
                .split('').sort().join(''))
        );
    }
    ,
    /**
     * @abstract greatestNum: This function, will retrieve the greatest posible number from a integer.
     * @param {Integer} num current number value
     */
    greatestNum: function greatestNum(num) {
        return (
            parseInt(num.toString().
                split('').sort().reverse().join(''))
        );
    },
    /**
    * @abstract displayStep: This function, will display the data for each step of the MagicNumber.
    * @param {Integer} num current MagicNumber value.
    * @param {Integer} greatestNumber highest posible number using the elements of the integer.
    * @param {Integer} lowestNumber lowest posible number using the elements of the integer.
    * @param {Integer} magicNumber substract from highest - lowest.
    * @param {Integer} numberLenght lenght of the fst number, used on padding.
    */
    displayStep: function displayStep(currentStep, num, greatestNumber, lowestNumber, magicNumber, numberLenght) {
       var paddedNumber = zeroPad(num, numberLenght);
       var paddedGreatest = zeroPad(greatestNumber, numberLenght);
       var paddedLowest = zeroPad(lowestNumber, numberLenght);
       var paddedMagicNumber = zeroPad(magicNumber, numberLenght);
   
       console.log(`[ Step ${currentStep} ]: > With ${paddedNumber} Greatest: ${paddedGreatest} / Lowest: ${paddedLowest} => Subtract ${paddedMagicNumber}`)
   },

    displayHeader: function displayHeader() {
        console.log('=============================================================================');
        console.log('Mapping of MagicNumber Tail Recursive Algorithm');
        console.log('=============================================================================\n');
    },

    displayFooter: function displayHeader(num, totalResolutionTime, totalSteps, magicNumber)  {
        console.log()
        console.log('=============================================================================');
        console.log(`- Number ${num}.`);
        console.log(`- Took ( ${totalResolutionTime} ) milliseconds.`);
        console.log(`- Was Resolved in ${totalSteps} Steps => ( ${magicNumber} ).`);
        console.log('=============================================================================');
    }


}
