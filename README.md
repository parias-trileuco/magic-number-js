# Magic Number JS with Terminal Recursion (Practising with Algorithms)

The exercise is the following, we name it The Magical Constant:

Take a number of 4 digits. The only restriction is that all the digits can't be the same. So, 2222 or 7777 are not allowed
Is important that digits lower than 1000 are allowed. In this case, you have to consider the number with as 0 padding to the left as you need. So numbers like 0003, 0047 or 0659 are also allowed.
With that number:
    Get the greatest number that you can make with all the 4 digits
    Get the lowest number that you can make with all the 4 digits

- Make the subtraction of the previous numbers: the greatest number - the lowest number
With the result, make the same. Get the greatest and the lowest number and subtract both numbers.
Repeat this process as many times as you need until the number got after the subtract is the same that the previous one got. This will be the magical number, and ALL the 4 digits numbers after this process reach this magical number, and always the same. So, there is ONLY ONE magical number if you take 4 digit numbers.

## Automatic Installation:

``` bash
# Install dependencies for server
make install

```

## Quick Start:

``` bash
# test magicNumber 
make test

# run magicNumber 
node magicNumber.js 8597

=============================================================================
MagicNumber Tail Recursive Algorithm
=============================================================================

[ Step 0 ]: > With 8597 Greatest: 9875 / Lowest: 5789 => Subtract 4086
[ Step 1 ]: > With 4086 Greatest: 8640 / Lowest: 0468 => Subtract 8172
[ Step 2 ]: > With 8172 Greatest: 8721 / Lowest: 1278 => Subtract 7443
[ Step 3 ]: > With 7443 Greatest: 7443 / Lowest: 3447 => Subtract 3996
[ Step 4 ]: > With 3996 Greatest: 9963 / Lowest: 3699 => Subtract 6264
[ Step 5 ]: > With 6264 Greatest: 6642 / Lowest: 2466 => Subtract 4176
[ Step 6 ]: > With 4176 Greatest: 7641 / Lowest: 1467 => Subtract 6174
[ Step 7 ]: > With 6174 Greatest: 7641 / Lowest: 1467 => Subtract 6174

=============================================================================
- Took ( 0.4946979992091656 ) milliseconds.
- Was Resolved in 7 Steps => ( 6174 ).
=============================================================================

```